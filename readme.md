# Lesson Task: 1.1 Working with Git

The purpose of this lesson is to get an intruduction to using Git.

1. Create a public repository hosted on GitLab (not GitHub).
2. Ensure that the repository has the following files to start with:
    - readme.md
    - .gitignore (matching your operating system)
    - an image of a cat or dog (your preference)
    - a text file called "old.txt" (Write anything inside it)
3. Do the following via DIFFERENT commits (each to their own):
    - Add a different picture (perhaps of a tree or car?)
    - Add a new markdown file ("picture.md"), its contents must describe the picture
    - Delete "old.txt"
    - Update "readme.md" to describe the purpose of the repository